﻿namespace WindowsLiderEntrega.Enumerations
{
    public class TipoTransaccionEnumeration
        : Enumeration
    {
        public static TipoTransaccionEnumeration Debito = new TipoTransaccionEnumeration(1, nameof(Debito));

        public TipoTransaccionEnumeration(int id, string name)
            : base(id, name)
        {
        }
    }
}
