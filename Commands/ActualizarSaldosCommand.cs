﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using WindowsLiderEntrega.Enumerations;
using WindowsLiderEntrega.Helpers;
using WindowsLiderEntrega.ServicioPrueba;

namespace WindowsLiderEntrega.Commands
{
    public class ActualizarSaldosCommand : ICommand
    {
        private readonly string _usuario;
        private readonly string _password;
        private ServiceClient _serviceClient;

        private readonly Action<int> _actualizarPorcentajeCalculado;

        public ActualizarSaldosCommand(Action<int> actualizarPorcentajeCalculado)
        {
            _usuario = ConfigurationManager.AppSettings["Usuario"];
            _password = ConfigurationManager.AppSettings["Password"]; ;
            _serviceClient = new ServiceClient();
            _actualizarPorcentajeCalculado = actualizarPorcentajeCalculado;
        }

        public void Execute()
        {
            try
            {
                var saldos = new List<Saldo>();
                var totalCuentasProcesadas = 0;

                var respuestaTransacciones = _serviceClient.GetData(_usuario, _password);

                var transaccionesCuentas = respuestaTransacciones.GroupBy(t => t.CuentaOrigen).ToList();
                var totalCuentas = transaccionesCuentas.Count();

                foreach (var transaccionesCuenta in transaccionesCuentas)
                {
                    var saldoCuenta = CalcularSaldo(transaccionesCuenta);

                    saldos.Add(saldoCuenta);

                    totalCuentasProcesadas++;

                    ActualizarProgreso(totalCuentas, totalCuentasProcesadas);
                }

                _serviceClient.SaveData(_usuario, _password, saldos.ToArray());
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private void ActualizarProgreso(int totalCuentas, int totalCuentasProcesadas)
        {
            var valorBarraProgreso = (int)(((float)totalCuentasProcesadas / totalCuentas) * 100);

            _actualizarPorcentajeCalculado(valorBarraProgreso);
        }

        private Saldo CalcularSaldo(IGrouping<long, Transaccion> transaccionesCuenta)
        {
            var saldo = new Saldo();
            var transacciones = transaccionesCuenta.AsEnumerable();
            var claveCifrado = _serviceClient.GetClaveCifradoCuenta(_usuario, _password, transaccionesCuenta.Key);

            saldo.CuentaOrigen = transaccionesCuenta.Key;

            foreach (var transaccion in transacciones)
            {
                var comision = SaldoHelper.CalcularComision(Convert.ToInt64(transaccion.ValorTransaccion));

                if (CifradoHelper.Desencripta(claveCifrado, transaccion.TipoTransaccion) != TipoTransaccionEnumeration.Debito.Name)
                {
                    saldo.SaldoCuenta -= transaccion.ValorTransaccion;
                }
                else
                {
                    saldo.SaldoCuenta += transaccion.ValorTransaccion - comision;
                }
            }

            return saldo;
        }
    }
}
