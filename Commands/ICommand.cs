﻿namespace WindowsLiderEntrega.Commands
{
    interface ICommand
    {
        void Execute();
    }
}
