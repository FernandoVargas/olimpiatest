﻿//PRUEBA OLIMPIA
//La función de la aplicación actual es calcular el saldo final de las cuentas de un "banco", para esto se consume un servicio que devuelve 
//las transacciones realizas a la cuentas.

//Paso 1: Hacer funcionar la aplicación. Debido al aumento de transacciones y al  colocar al servicio con SSL la aplicación actual esta fallando.
//Paso 2: Estructurar mejor el codigo. Uso de patrones, buenas practicas, etc.
//Paso 3: Optimizar el codigo, como se menciono en el paso 1 el aumento de transacciones ha causado que el calculo de los saldos se demore demasiado.
//Paso 4: Adicionar una barra de progreso al formulario. Actualizar la barra con el progreso del proceso, evitando bloqueos del GUI.

using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsLiderEntrega.Commands;

namespace WindowsLiderEntrega
{
    public partial class Form1 : Form
    {
        private Timer _tiempoBarraProgreso;
        private int _valorBarraProgreso;
        private ICommand _actualizarSaldosCommand;

        public Form1()
        {
            InitializeComponent();
            InicializarTiempoBarraProgreso();
        }

        private void InicializarTiempoBarraProgreso()
        {
            _tiempoBarraProgreso = new Timer();
            _tiempoBarraProgreso.Tick += new EventHandler(ActualizarTiempoBarraProgreso);
            _tiempoBarraProgreso.Interval = 50;
        }

        private void ActualizarTiempoBarraProgreso(object sender, EventArgs e)
        {
            BarraProgreso.Value = _valorBarraProgreso;
        }

        private async void btnCalcular_Click(object sender, EventArgs e)
        {
            btnCalcular.Enabled = false;
            _valorBarraProgreso = 0;

            _tiempoBarraProgreso.Start();
            var sw = System.Diagnostics.Stopwatch.StartNew();

            _actualizarSaldosCommand = new ActualizarSaldosCommand(ActualizarPorcentajeCalculado);
            await Task.Factory.StartNew(_actualizarSaldosCommand.Execute);

            sw.Stop();

            BeginInvoke((MethodInvoker)delegate
            {
                lblTiempoTotal.Text = sw.ElapsedMilliseconds.ToString();
                _tiempoBarraProgreso.Stop();
                BarraProgreso.Value = 100;
                btnCalcular.Enabled = true;
            });
        }

        public void ActualizarPorcentajeCalculado(int porcentaje)
        {
            _valorBarraProgreso = porcentaje;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
